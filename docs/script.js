const TWITCH_CLIENT_ID = '3r4wdy963ayhdmb75a3bmemkha6c10';
const DEFAULT_STATE = {
  channels: [],
  showSearch: false,
  showDay: false,
  showTime: false,
  results: [],
  time: [],
  activeTime: null,
  day: [],
  activeDay:null,
  videos: {},
  streams: []
};
const STATE_NAME = 'state';
const SPACEBAR_KEY_CODE = [0, 32];
const ENTER_KEY_CODE = 13;
const DOWN_ARROW_KEY_CODE = 40;
const UP_ARROW_KEY_CODE = 38;
const ESCAPE_KEY_CODE = 27;
const APP_NAME = 'Twitch Schedules';

function debug(state, type){
  // console.log(`'${type}' update`);
}

function createState(state){
  Object.defineProperty(state, 'onUpdate', {
    value: state.onUpdate || [debug],
    enumerable: false
  });
  state.showSearch = false;
  state.showDay = false;
  state.showTime = false;
  state.results = [];
  state.time = getTime();
  state.activeTime = getActiveTime();
  state.day = getDay();
  state.activeDay = getActiveDay();
  state.streams = [];
  if(!state.videos){
    state.videos = {};
  }
  if(!state.channels){
    state.channels = [];
  }
  Object.keys(state.videos).forEach(id => {
    Object.keys(state.videos[id]).forEach(d => {
      state.videos[id][d] = state.videos[id][d].map(pair => pair.map(p => new Date(p)))
    })
  });
  Object.defineProperty(state, 'container', {
    value: containerElm(state),
    enumerable: false
  });
  return state;
}

function getState(){
  let state = DEFAULT_STATE;
  try {
    state = JSON.parse(localStorage.getItem(STATE_NAME)) || state;
  } catch (e) {
    //
  }
  return createState(state);
}

function setState(state){
  try {
    localStorage.setItem(STATE_NAME, JSON.stringify(state));
  } catch (e) {
      //
  }
}

function updateState(state, type, ...args){
  setState(state);
  state.onUpdate.forEach(u => u(state, type, ...args));
}

function elm({
  name = 'div',
  attr = {},
  cls = [],
  content = null
} = {}){
  const e = document.createElement(name);
  Object.keys(attr).forEach(k => {
    e.setAttribute(k, attr[k]);
  });
  cls = Array.isArray(cls) ? cls : cls.split(',');
  cls.forEach(c => e.classList.add(c));
  if(typeof content === 'string'){
    e.innerHTML = content;
  }else if(Array.isArray(content)){
      content.forEach(c => e.appendChild(c));
  }else if(content instanceof Node){
    e.appendChild(content);
  }
  return e;
}

function formatHour(value, half){
  const i = value;
  const m = {};
  if(half){
    m.a = half + ' AM';
    m.p = half + ' PM';
  }else{
    m.a = ' AM';
    m.p = ' PM';
  }
  value = value % 12;
  return value === 0 ? 12 + (i < 12 || i === 24 ? m.a : m.p) : (i < 12 ? value + m.a : value + m.p);
}

function formatDay(date){
  return new Intl.DateTimeFormat('en-US', {weekday:'short'}).format(date);
}

function parseTime(time){
  let i = parseInt(time.replace(' AM', '').replace(' PM', ''));
  if(time.indexOf('PM') !== -1){
    i = i === 12 ? 12 : i + 12;
  }else{
    i = i === 12 ? 0 : i;
  }
  return i;
}

function getHour(){
  return new Date().getHours();
}

function getTime(){
  const times = [];
  let i = 0;
  while(i < 24){
    times.push(formatHour(i));
    i++;
  }
  return times;
}

function getActiveTime(){
  return formatHour(getHour());
}

function getDay(){
  const days = [];
  const today = new Date();
  let next = new Date();
  let i = 0;
  while(i < 7){
    next.setDate(today.getDate() + i);
    days.push(formatDay(next));
    i++;
  }
  return days;
}

function getActiveDay(){
  return formatDay(new Date());
}

function searchChannels(query, cb){
  fetch(
    `https://api.twitch.tv/kraken/search/channels?query=${query}&limit=5`,
    {
      method: 'GET',
      headers: {
        'client-id': TWITCH_CLIENT_ID,
        'Accept': 'application/vnd.twitchtv.v5+json'
      }
    }
  )
  .then(r => r.json())
  .then(data => {
    try {
      cb(null, data);
    } catch (e) {
      console.log(e);
    }
  })
  .catch(err => cb(err))
}

function searchFormElm(state){
  const input = elm({
    name:'input',
    cls:'input,search-input',
    attr: {
      type:'search',
      autocomplete: 'off',
      placeholder: 'Channel name',
      role: 'combobox',
      id: 'search',
      'aria-owns': 'autocomplete-options--destination',
      'autocapitalize': 'none',
      'aria-autocomplete': 'list',
      'aria-expanded': 'false'
    }
  });
  const mag = svgElm('search');
  const back = elm({name:'button', cls:'button,back-button', attr: {type:'button'}, content:[textElm('Cancel')]});
  const button = elm({name:'button', cls:'button,clear-button', attr: {type:'button'}, content: [svgElm('remove')]});
  const form = elm({name:'form', cls:'form', content: [back, input, mag, button]});
  state.onUpdate.push((s, type)=>{
    if(type === 'search' && !s.search){
      input.value = '';
      button.classList.remove('show');
    }
  });
  back.addEventListener('click', ()=>{
    state.search = '';
    updateState(state, 'search');
    toggleSearch(state, false);
  });
  input.addEventListener('keydown', evt => {
    switch (evt.keyCode) {
      case DOWN_ARROW_KEY_CODE:
      case UP_ARROW_KEY_CODE:
        // focus item
        break;
      case ESCAPE_KEY_CODE:
        clearSearch(state);
        toggleSearch(state, false);
        break;
      default:
        //
    }
  });
  input.addEventListener('blur', ()=>{
    if(!state.search){
      toggleSearch(state, false);
    }
  });
  button.addEventListener('click', ()=>{
    input.focus();
    clearSearch(state);
  });
  input.addEventListener('input', ()=>{
    if(input.value){
      button.classList.add('show');
    }else{
      button.classList.remove('show');
    }
    state.search = input.value;
    updateState(state, 'search');
  });
  form.addEventListener('submit', (evt)=>{
    evt.preventDefault();
    if(!input.value){
      return;
    }
    state.search = input.value;
    updateState(state, 'search');
  });
  return form;
}

function searchResultsElm(state){
  const results = elm({cls:'list', attr:{role:'listbox'}});
  let debounce;
  state.onUpdate.push((s, type)=>{
    if(debounce){
      clearTimeout(debounce);
      debounce = null;
    }
    if(type === 'search'){
      if(!state.search){
        state.results = [];
        updateState(state, 'results');
        return;
      }
      state.results = [{_searchMessage: 'Looking for channels...'}];
      updateState(state, 'results');
      debounce = setTimeout(()=>{
        debounce = null;
        if(state.search){
          searchChannels(state.search, (err, data)=>{
            if(err){
              console.log(err);
              return;
            }
            state.results = data.channels && data.channels.length ? data.channels : [{_searchMessage: 'No results : ('}];
            updateState(state, 'results');
          });
        }
      }, 500);
    }
    if(type === 'results'){

      const frag = document.createDocumentFragment();
      state.results.forEach(channel => {
        if(channel._searchMessage){
          const msg = elm({cls: 'option', content: channel._searchMessage});
          frag.appendChild(msg);
          return;
        }
        // make this a function
        const result = elm({cls: 'option', content: channel['display_name'], attr:{tabindex: 0, role:'option'}});
        result.addEventListener('click', ()=>{
          addChannel(state, channel);
        });
        result.addEventListener('keydown', (evt)=>{
          if(evt.keyCode === ENTER_KEY_CODE){
            addChannel(state, channel);
          }
        });
        frag.appendChild(result);
      });
      requestAnimationFrame(()=>{
        results.innerHTML = '';
        results.appendChild(frag);
      });
    }
  });
  return results;
}

// offcanvas
function searchElm(state){
  const form = searchFormElm(state);
  const results = searchResultsElm(state);
  const search = elm({cls:'search,offcanvas', content:[form, results]});
  state.onUpdate.push((s, type)=>{
    if(type === 'showSearch'){
      search.classList.toggle('show', state.showSearch);
      form.querySelector('input').focus();
    }
  });
  return search;
}

function nowElm(state){
  const button = buttonElm(state, {text:'Now', type: 'now'});
  button.addEventListener('click', ()=>{
    const date = new Date();
    state.activeDay = formatDay(date);
    state.activeTime = formatHour(date.getHours());
    updateState(state, 'activeTime');
    updateState(state, 'activeDay');
    updateState(state, 'videos');
  });
  return button;
}

function dayElm(state){
  const button = offcanvasButtonElm(state, {type:'day',icon:'arrow', text: state.activeDay}, toggleDay, 'activeDay');
  const offcanvas = offcanvasElm(state, 'showDay', offcanvasListElm(state, 'day', 'activeDay', toggleDay));
  const wrap = offcanvasWrapperElm(state, button, offcanvas, 'showDay');
  wrap.classList.add('offcanvas-wrap--small');
  return wrap;
}

function timeElm(state){
  const button = offcanvasButtonElm(state, {type:'time',icon:'arrow', text: state.activeTime}, toggleTime, 'activeTime');
  const offcanvas = offcanvasElm(state, 'showTime', offcanvasListElm(state, 'time', 'activeTime', toggleTime));
  const wrap = offcanvasWrapperElm(state, button, offcanvas, 'showTime');
  wrap.classList.add('offcanvas-wrap--small');
  return wrap;
}

function dayTimeElm(state){
  return elm({
    cls:'day-time',
    content: [
      dayElm(state),
      timeElm(state),
      nowElm(state)
    ]
  });
}

function optionElm(state, active, item, toggle){
  const option = elm({cls: 'option', content: item, attr:{tabindex: 0, role:'option'}});
  option.addEventListener('click', ()=>{
    state[active] = item;
    updateState(state, active);
    toggle(state, false);
    updateState(state, 'videos');// hax
  });
  option.addEventListener('keydown', (evt)=>{
    if(evt.keyCode === ENTER_KEY_CODE){
      state[active] = item;
      updateState(state, active);
      toggle(state, false);
    }
    if(evt.keyCode === ESCAPE_KEY_CODE){
      toggle(state, false);
    }
  });
  return option;
}

function offcanvasListElm(state, list, active, toggle){
  const listbox = elm({
    cls:[list, 'list'],
    attr:{role:'listbox'},
    content: state[list].map(item => {
      const o = optionElm(state, active, item, toggle);
      o.setAttribute('data-val', item);
      if(item === state[active]){
        o.classList.add('active');
      }
      return o;
    })
  });
  state.onUpdate.push((s, type)=>{
    if(type === list){
      listbox.innerHTML = '';
      const frag = document.createDocumentFragment();
      state[list].forEach(item => {
        const o = optionElm(state, active, item, toggle);
        o.setAttribute('data-val', item);
        if(item === state[active]){
          o.classList.add('active');
        }
        frag.appendChild(o);
      });
      requestAnimationFrame(()=>{
        listbox.appendChild(frag);
      });
    }
    if(type === active){
      let o = listbox.querySelector('.active');
      if(o){
        o.classList.remove('active');
      }
      o = listbox.querySelector(`[data-val="${state[active]}"]`);
      if(o){
        o.classList.add('active');
      }
    }
  });
  return listbox;
}

function offcanvasButtonElm(state, config, toggle, active){
  const button = buttonElm(state, config);
  button.addEventListener('click', ()=>{
    toggle(state);
  });
  button.addEventListener('keydown', evt => {
    switch (evt.keyCode) {
      case ESCAPE_KEY_CODE:
        toggle(state, false);
        break;
      default:
      //
    }
  });
  state.onUpdate.push((s, type)=>{
    if(type === active){
      const span = button.querySelector('span');
      if(span){
        span.textContent = state[active];
      }
    }
  });
  return button;
}

function offcanvasElm(state, type, list){
  const offcanvas = elm({cls:[type, 'offcanvas'], content:[list]});
  state.onUpdate.push((s, t)=>{
    if(t === t){
      offcanvas.classList.toggle('show', state[type]);
    }
  });
  return offcanvas;
}

function offcanvasWrapperElm(state, button, offcanvas, isActive){
  const wrap = elm({cls:'offcanvas-wrap', content: [button, offcanvas]});
  state.onUpdate.push((s, type)=>{
    if(type === isActive){
      wrap.classList.toggle('active', state[isActive]);
    }
  });
  return wrap;
}

function addButtonWrapperElm(state){
  return offcanvasWrapperElm(state, addStreamButton(state), searchElm(state), 'showSearch');
}

function channelTagElm(state, channel){
  const button = buttonElm(state,{
    text: channel.display_name,
    type:'remove',
    icon:'remove'
  });
  button.setAttribute('data-id', channel._id);
  button.addEventListener('click', ()=>{
    removeChannel(state, channel);
  });
  return button;
}

function channelTagsElm(state){
  const tags = elm({
    cls: 'tags',
    content: state.channels.map(c => channelTagElm(state, c))
  });
  state.onUpdate.push((s, type, channel)=>{
    if(type === 'removeChannel'){
      const b = tags.querySelector(`[data-id="${channel._id}"]`);
      if(b){
        b.remove();
      }
    }
    if(type === 'addChannel'){
      tags.appendChild(channelTagElm(state, channel));
    }
  });
  return tags;
}

function backArrow(){
  const svg = svgElm('arrow');
  svg.classList.add('svg--arrow-back');
  return svg;
}

function forwardArrow(){
  const svg = svgElm('arrow');
  svg.classList.add('svg--arrow-forward');
  return svg;
}

function svgElm(name){
  const use = document.createElementNS('http://www.w3.org/2000/svg', 'use');
  use.setAttributeNS('http://www.w3.org/1999/xlink', 'href', `#svg-${name}`);
  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svg.appendChild(use);
  svg.classList.add('svg', `svg--${name}`);
  return svg;
}

function textElm(val){
  return document.createTextNode(val || '');
}

function toggleSearch(state, val = null){
  state.showSearch = val !== null ? val : !state.showSearch;
  updateState(state, 'showSearch');
}

function clearSearch(state){
  state.search = '';
  state.results = [];
  updateState(state, 'search');
  updateState(state, 'results');
}

function addChannel(state, channel){
  if(!state.channels.some(c => c._id === channel._id)){
    state.channels.push(channel);
    updateState(state, 'addChannel', channel);
  }
  clearSearch(state);
  toggleSearch(state, false);
}

function removeChannel(state, channel){
  state.channels.splice(state.channels.indexOf(channel), 1);
  updateState(state, 'removeChannel', channel);
}

function toggleDay(state){
  state.showDay = !state.showDay;
  updateState(state, 'showDay');
}

function toggleTime(state){
  state.showTime = !state.showTime;
  updateState(state, 'showTime');
}

function getTimeOffset(state){
  return state.columns.childNodes[2].offsetLeft >= state.columns.clientWidth ? 1 : 2;
}

function getNextDay(state){
  const i = state.day.indexOf(state.activeDay);
  return i + 1 === state.day.length ? state.day[0] : state.day[i+1];
}

function nextTime(state){
  // const offset = getTimeOffset(state); // 1 or 2 hours depending on what is visible
  const offset = 1;
  let parsed = parseTime(state.activeTime);
  parsed = parsed > 23 - offset ? 0 : parsed + offset;
  state.activeTime = formatHour(parsed);
  updateState(state, 'activeTime');
  if(parsed === 0){
    const i = state.day.indexOf(state.activeDay);
    state.activeDay = i + 1 === state.day.length ? state.day[0] : state.day[i+1];
    updateState(state, 'activeDay');
  }
  updateState(state, 'videos');
}

function prevTime(state){
  // const offset = getTimeOffset(state);
  const offset = 1;
  let parsed = parseTime(state.activeTime);
  parsed = parsed - offset < 0 ? 23 : parsed - offset;
  state.activeTime = formatHour(parsed);
  updateState(state, 'activeTime');
  if(parsed === 23){
    const i = state.day.indexOf(state.activeDay);
    state.activeDay = i === 0 ? state.day[state.day.length - 1] : state.day[i-1];
    updateState(state, 'activeDay');
  }
  updateState(state, 'videos');// hax
}

function buttonElm(state, config){
  const content = [];
  const cls = ['button'];
  if(config.text){
    content.push(elm({name:'span', content: config.text}));
  }
  if(config.icon){
    content.push(svgElm(config.icon));
  }
  if(config.type){
    cls.push(`button--${config.type}`);
  }
  if(config.cls){
    cls.push(config.cls);
  }
  return elm({
    name:'button',
    attr:{type:'button'},
    cls,
    content
  });
}

function addStreamButton(state){
  const button = buttonElm(state, {type:'add',icon:'add',text:'Add channel'});
  button.addEventListener('click', ()=>{
    toggleSearch(state);
  });
  button.addEventListener('keydown', evt => {
    switch (evt.keyCode) {
      case ESCAPE_KEY_CODE:
        toggleSearch(state, false);
        break;
      default:
      //
    }
  });
  return button;
}

function convertMS( milliseconds ) {
  var day, hour, minute, seconds;
  seconds = Math.floor(milliseconds / 1000);
  minute = Math.floor(seconds / 60);
  seconds = seconds % 60;
  hour = Math.floor(minute / 60);
  minute = minute % 60;
  day = Math.floor(hour / 24);
  hour = hour % 24;
  return {
    day: day,
    hour: hour,
    minute: minute,
    seconds: seconds
  };
}

function formatDuration(pair){
  const t = convertMS(pair[1] - pair[0]);
  return `${t.hour < 10 ? '0' + t.hour : t.hour}:${t.minute < 10 ? '0' + t.minute : t.minute}:${t.seconds < 10 ? '0' + t.seconds : t.seconds}`;
}

function channelIsLive(state, id){
  return state.streams.indexOf(id) !== -1;
}

function channelImageElm(state, channel){
  const liveCls = ['channel-col__live'];
  if(channelIsLive(state, channel._id)){
    liveCls.push('show');
  }
  return elm({
    cls:'channel-col__img-wrap',
    content:[
    elm({
      name: 'img',
      cls: 'channel-img',
      attr: {
        src: channel.logo
      }
    }),
    elm({content: 'Live',cls: liveCls}),
  ]});
}

function filterChannelVideosByActiveTime(state, videos, startTime, cols){
  return videos.filter(dates => {
    const s = new Date(dates[0]);
    s.setHours(startTime);
    s.setMinutes(0);
    s.setSeconds(0);
    const e = new Date(dates[0]);
    e.setHours(startTime + cols / 2 - 1);
    e.setMinutes(30);
    e.setSeconds(0);
    const start = dates[0];
    const end = dates[1];
    return start >= s && start < e || start < s && end >= s;
  });
}

function channelRow(state, channel){
  const row = elm({cls:'channel-row', attr:{'data-id': channel._id}});
  const cols = getTimeOffset(state) * 2;
  const startTime = parseTime(state.activeTime);
  if(state.videos[channel._id] && state.videos[channel._id][state.activeDay]){
    let times = filterChannelVideosByActiveTime(state, state.videos[channel._id][state.activeDay], startTime, cols);
    // const times = state.videos[channel._id][state.activeDay].filter(dates => {
    //   const s = new Date(dates[0]);
    //   s.setHours(startTime);
    //   s.setMinutes(0);
    //   s.setSeconds(0);
    //   const e = new Date(dates[0]);
    //   e.setHours(startTime + cols / 2);
    //   e.setMinutes(0);
    //   e.setSeconds(0);
    //   const start = dates[0];
    //   const end = dates[1];
    //   return start >= s && start < e || start < s && end >= s;
    // });
    if(startTime === 23 && cols === 4 && state.videos[channel._id][getNextDay(state)]){
      times = times.concat(filterChannelVideosByActiveTime(state, state.videos[channel._id][getNextDay(state)],0,2));
    }
    times.forEach(pair => {
      let startColumn;
      let endColumn;
      let compare;
      for(let i = 0; i < cols; i++){
        compare = startTime + i / 2;
        if(startTime === 23 && pair[0].getHours() === 0){
          startColumn = 3; // hax?
          endColumn = 5;
        }
        if(pair[0].getHours() + pair[0].getMinutes() / 60 <= compare && !startColumn){
          startColumn = i + 1;
        }
        if(pair[1].getHours() + pair[1].getMinutes() / 60 >= compare){
          endColumn = i + 2;
        }
      }
      const style = `grid-column: ${startColumn} / ${endColumn};`;
      const col = elm({
        name: 'a',
        cls: 'channel-col',
        attr: {
          href: channel.url,
          target: '_blank',
          style
        },
        content: [
          channelImageElm(state, channel),
          elm({cls:'channel-col__info', content: [
            elm({content: channel.display_name, cls: 'channel-col__name'}),
            elm({content: formatDuration(pair), cls: 'channel-col__duration'})
          ]})
        ]
      });
      row.appendChild(col);
    });
  }
  return row;
}

function hourElm(state, content){
  return elm({cls:'hour-column', content});
}

function hoursElm(state){
  const div = elm({cls:'hour-columns'});
  const append = ()=>{
    let i = 1;
    let h = parseTime(state.activeTime) - 1;
    let half = false;
    while(i < 5){
      if(i % 2 === 0){
        half = true;
      }else{
        half = false;
        h = h + 1;
      }
      div.appendChild(hourElm(state, formatHour(h, half ? ':30' : ':00')));
      i++;
    }
  };
  append();
  state.onUpdate.push((s, type)=>{
    if(type === 'activeTime'){
      div.innerHTML = '';
      append();
    }
  });
  Object.defineProperty(state, 'columns', {
    value: div,
    enumerable: false
  });
  return div;
}

function hoursControlElm(state){
  const backButton = buttonElm(state, {icon: 'arrow', type:'arrow-back', cls:'button--round'});
  const forwardButton = buttonElm(state, {icon: 'arrow', type:'arrow-forward', cls:'button--round'});
  const div = elm({
    cls:'hour-control',
    content:[
      backButton,
      hoursElm(state),
      forwardButton,
    ]
  });
  backButton.addEventListener('click', ()=>{
    prevTime(state);
  });
  forwardButton.addEventListener('click', ()=>{
    nextTime(state);
  });
  return div;
}

function fetchChannelVideos(channels, cb){
  Promise.all(channels.map(channel => {
    return fetch(
      `https://api.twitch.tv/kraken/channels/${channel._id}/videos?limit=100&broadcast_type=archive`,
      {
        method: 'GET',
        headers: {
          'client-id': TWITCH_CLIENT_ID,
          'Accept': 'application/vnd.twitchtv.v5+json'
        }
      }
    )
    .then(r => r.json())
    .then(d => ({...d, channel}))
  }))
  .then(results => {
    try {
      cb(null, results);
    }catch (e){
      console.log(e);
    }
  })
  .catch(e => cb(e))
}

function isOverlap(list){
  const mapped = [];
  return list.some()
}

function mapChannelVideos(state, results){
  const map = Object.assign({}, state.videos);
  const today = new Date();
  results.forEach(result => {
    const series = result.videos.map(video => {
      const startTime = new Date(video['created_at']);
      // startTime.setFullYear(today.getFullYear());
      // startTime.setMonth(today.getMonth());
      // startTime.setDate(today.getDate());
      const endTime = new Date();
      endTime.setTime(startTime.getTime() + (video.length * 1000));
      return [
        startTime,
        endTime
      ];
    });
    series.forEach(pair => {
      if(pair[0].getDate() < pair[1].getDate()){
        const lastEnd = new Date(pair[1]);
        const newStart = new Date(lastEnd);
        pair[1] = new Date(pair[0]);
        pair[1].setHours(23);
        pair[1].setMinutes(59);
        pair[1].setSeconds(59);
        newStart.setHours(0);
        newStart.setMinutes(0);
        newStart.setSeconds(0);
        series.push([newStart, lastEnd]);
      }
    });
    state.day.forEach(day => {
      let firstDay;
      const videosByDay = series.filter(pair => {
        return formatDay(pair[0]) === day;
      }).map(pair => {
        if(!firstDay){
          firstDay = new Date(today);
          firstDay.setDate(1);
          while (formatDay(firstDay) !== day) {
            firstDay.setDate(firstDay.getDate() + 1);
          }
        }
        return pair.map(d => {
          d.setFullYear(firstDay.getFullYear());
          d.setMonth(firstDay.getMonth());
          d.setDate(firstDay.getDate());
          return d;
        });
      }).sort((a,b)=>{
        if(a[0] > b[0] ){
          return 1;
        }
        if(a[0] < b[0]){
          return -1;
        }
        return 0;
      });
      const res = [];
      if(videosByDay.length){
        res.push(videosByDay.shift());
      }
      videosByDay.forEach((pair, i) => {
        if(pair[0] < res[res.length - 1][1]){
          if(pair[1] > res[res.length - 1][1]){
            res[res.length - 1][1] = pair[1];
          }
        }else{
          res.push(pair);
        }
      });
      if(!map[result.channel._id]){
        map[result.channel._id] = {};
      }
      map[result.channel._id][day] = res;
    });
  });
  state.videos = map;
  updateState(state, 'videos');
}

function hideOrShow(state, slider){
  const now = new Date();
  const m = now.getMinutes();
  let h = now.getHours();
  let parsed = parseTime(state.activeTime);
  const day = formatDay(now);
  const offset = getTimeOffset(state);
  if(day === state.activeDay && h >= parsed && h < parsed + offset){
    slider.classList.add('show');
  }else{
    slider.classList.remove('show');
  }

  const percent =  ((h + m / 60) - parsed) / 2;

  let w;
  if(slider.parentNode){
    w = slider.parentNode.clientWidth;
  }else{
    w = 0;
  }
  h = h % 12;
  h = h === 0 ? 12 : h;
  parsed = parsed % 12;
  parsed = parsed === 0 ? 12 : parsed;
  const x = Math.max(0, w * percent);
  requestAnimationFrame(()=>{
    slider.childNodes[0].textContent = (h === 0 ? 12 : h) + ':' + (m < 10 ? '0' + m : m);
    slider.setAttribute('style', `transform:translateX(${x}px)`);
  });
}

function slideElm(state){
  const e = elm({cls:'slider',content:[elm({cls:'slider-text'})]});
  state.onUpdate.push((e, type)=>{
    if(e === 'activeTime'){
      hideOrShow(state, e);
    }
  });
  setInterval(()=>{
    hideOrShow(state, e);
  }, 500);
  hideOrShow(state, e);
  return e;
}

function fetchStreams(state, cb){
  fetch(
    `https://api.twitch.tv/kraken/streams/?channel=${state.channels.map(c => c._id).join(',')}`,
    {
      method: 'GET',
      headers: {
        'client-id': TWITCH_CLIENT_ID,
        'Accept': 'application/vnd.twitchtv.v5+json'
      }
    }
  )
  .then(r => r.json())
  .then(data => {
    try {
      cb(null, data);
    } catch (e) {
      console.log(e);
    }
  })
  .catch(e => cb(e))
}

function mapLiveStreams(state, data){
  const live = [];
  data.streams.forEach(s => {
    if(s.channel && live.indexOf(s.channel._id) === -1){
      live.push(s.channel._id);
    }
  });
  state.streams = live;
  updateState(state, 'streams');
}

function scheduleElm(state){
  const schedule = elm({cls:'inner', content:state.channels.map(c => channelRow(state, c))});
  const sched = elm({cls:'schedule', content: [
    schedule,
    slideElm(state)
  ]});
  fetchChannelVideos(state.channels, (err, data)=>{
    mapChannelVideos(state, data);
  });
  fetchStreams(state, (err, data)=>{
    mapLiveStreams(state, data);
  });
  let checkStreams = setInterval(()=>{
    fetchStreams(state, (err, data)=>{
      mapLiveStreams(state, data);
    });
  }, 1000 * 60 * 3);
  state.onUpdate.push((s, type, channel)=>{
    if(type === 'videos'){
      const frag = document.createDocumentFragment();
      state.channels.map(c => frag.appendChild(channelRow(state, c)));
      requestAnimationFrame(()=>{
        schedule.innerHTML = '';
        schedule.appendChild(frag);
      });
    }
    if(type === 'streams'){
      Array.from(schedule.querySelector(`.channel-row`)).forEach((row)=>{
        const id = parseInt(row.getAttribute('data-id'));
        const l = row.querySelector('.channel-col__live');
        if(l){
          l.classList.toggle('show', channelIsLive(state, id));
        }
      });
    }
    if(type === 'addChannel'){
      fetchChannelVideos([channel], (err, data)=>{
        mapChannelVideos(state, data);
      });
      fetchStreams(state, (err, data)=>{
        mapLiveStreams(state, data);
      });
      schedule.appendChild(channelRow(state, channel));
    }
    if(type === 'removeChannel'){
      const c = schedule.querySelector(`[data-id="${channel._id}"]`);
      if(c){
        c.remove();
      }
    }
  });
  return sched;
}

function headerElm(state){
  const header = elm({
    name:'header',
    cls:'header',
    content:[
      svgElm('logo'),
      elm({name:'h1',content:APP_NAME})
    ]
    });
  return header;
}

function containerElm(state){
  if(state.container){
    return state.container;
  }
  const container = elm({cls: 'container', content: [
    headerElm(state),
    addButtonWrapperElm(state),
    channelTagsElm(state),
    dayTimeElm(state),
    hoursControlElm(state),
    scheduleElm(state),
    backDropElm(state)
  ]});

  document.body.appendChild(container);
  return container;
}

const BACKDROP_ACTIONS = [
  'showSearch',
  'showDay',
  'showTime'
];
function backDropElm(state){
  const backdrop = elm({cls: 'backdrop'});
  state.onUpdate.push((s, type)=>{
    if(BACKDROP_ACTIONS.some(a => a === type)){
      backdrop.classList.toggle('show', state[type]);
    }
  });
  backdrop.addEventListener('click', (evt)=>{
    evt.preventDefault();
    BACKDROP_ACTIONS.some(action => {
      if(state[action]){
        state[action] = false;
        clearSearch(state); //hax
        updateState(state, action);
        return true;
      }
    });
  });
  return backdrop;
}

const STATE = getState(); // get state once at runtime

document.addEventListener('visibilitychange', ()=>{
  if(document.visibilityState === 'visible'){
    updateState(STATE, 'visibilitychange');
  }
});

window.addEventListener('pageshow', ()=>{
  updateState(STATE, 'pageshow');
  let timeout;
  window.addEventListener('resize', ()=>{
    clearTimeout(timeout);
    timeout = setTimeout(()=>{
      updateState(STATE, 'videos');
    }, 500);
  });
});
